
import subprocess


def apertium(inp, srcdir, mode):
    """ (String String String) -> String

    Run inp through 'apertium -d <srcdir> <mode>' command and return
    the result.
    """
    res = subprocess.run(['apertium', '-d', srcdir, mode],
                         stdout=subprocess.PIPE,
                         input=inp.encode('utf-8'))
    return res.stdout.decode('utf-8')

def test_apertium():
    assert apertium(".", APERTIUM_LANGS_SRC + "apertium-tat", "tat-morph") ==\
        "^../..<sent>$"
