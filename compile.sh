#!/bin/bash

cd apertium-languages/
cd "$1"
pwd
git checkout master
git pull && ./autogen.sh && make clean && make
