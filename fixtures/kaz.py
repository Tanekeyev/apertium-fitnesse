
import subprocess
from waferslim.converters import TableTableConstants as TTC
import lexc2dix
import bs4 as bs
from streamparser.streamparser import parse

APERTIUM_LANGS_SRC = '../../apertium-languages/'
file_dic = '/home/gaba/apertium-all/apertium-languages/apertium-kaz/apertium-kaz.kaz.dix'
LANG1 = 'kaz'
COLUMNS = 5




class Kaz(object):

    def do_table(self, table_rows):
        res = []
        for row in table_rows:
            res_row = []
            for lang, chunk in zip([LANG1], chunks(row, COLUMNS)):
                surf, lem, tags, aff, lex = chunk
                srcdir = APERTIUM_LANGS_SRC + 'apertium-' + lang
                mode = lang + '-morph'
                iac = is_analysed_correctly(surf, lem, tags.split(), srcdir,
                                            mode ,aff)
                if iac == "TrueTrue":
                    res_row.extend([TTC.cell_no_change(),
                                    TTC.cell_correct(lem),
                                    TTC.cell_correct(tags),
                                    TTC.cell_correct(aff),
                                    TTC.cell_no_change()])
                elif iac == "TrueFalse":
                    res_row.extend([TTC.cell_no_change(),
                                    TTC.cell_correct(lem),
                                    TTC.cell_correct(tags),
                                    TTC.cell_incorrect(aff),
                                    TTC.cell_no_change()])
                elif iac == "lemmaTrue":
                    res_row.extend([TTC.cell_no_change(),
                                    TTC.cell_incorrect(lem),
                                    TTC.cell_correct(tags),
                                    TTC.cell_correct(aff),
                                    TTC.cell_no_change()])
                elif iac == "lemmaFalse":
                    res_row.extend([TTC.cell_no_change(),
                                    TTC.cell_incorrect(lem),
                                    TTC.cell_correct(tags),
                                    TTC.cell_incorrect(aff),
                                    TTC.cell_no_change()])    
                elif iac == "tagsTrue":
                    res_row.extend([TTC.cell_no_change(),
                                    TTC.cell_correct(lem),
                                    TTC.cell_incorrect(tags),
                                    TTC.cell_correct(aff),
                                    TTC.cell_no_change()])
                elif iac == "tagsFalse":
                    res_row.extend([TTC.cell_no_change(),
                                    TTC.cell_correct(lem),
                                    TTC.cell_incorrect(tags),
                                    TTC.cell_incorrect(aff),
                                    TTC.cell_no_change()])
                
                elif iac == "both":
                    res_row.extend([TTC.cell_no_change(),
                                    TTC.cell_incorrect(lem),
                                    TTC.cell_incorrect(tags),
                                    TTC.cell_no_change(),
                                    TTC.cell_no_change()])
            res.append(tuple(res_row))
        return tuple(res)

def lexcdic2dic(file_dic):
    d = {}
    f = open(file_dic , 'r')
    source =f.read()
    soup = bs.BeautifulSoup(source , 'lxml')
    for i, k in enumerate(soup.find_all('e')):
        word = k.get('lm')
        try:
            lexc = k.find_all('par')[0].get('n')
        except:
            lexc = ''
        d.update({word:lexc})
    for i in ";«“—][\.–,-/?)”»(:!":
        d[i] = '#'
    return d

dict_acc = lexcdic2dic(file_dic)

def is_analysed_correctly(surf, lem, tags, srcdir, mode , aff):
    """(String String (List of String) String String) -> True OR
                                                         OR "lemma"
                                                         OR "tags"
                                                         OR "both"

    Return True if the expected analysis (the lemma lem + tags) is among the
    analyses we get when we pass the surface form surf through the
    `apertium -d <srcdir> <mode>' command, "lemma" if there is one analysis with
    matching tags (i.e. the problem should be with the lemma) and "tags" if
    the lem is among the analyses, but with different tags and "both" if both
    don't match.
    """
    
        

    lus = list(parse(apertium(surf, srcdir, mode)))
    
    try:
        acc_value = dict_acc[lem]
    except:
        acc_value = ''

    if any([lem == r[0].baseform and  (''.join(tags) == ''.join(r[0].tags) or  tags == r[0].tags) for lu in lus for r in lu.readings]):
        if (aff == acc_value):
            return "TrueTrue"
        else:
            return "TrueFalse"
    elif any([lem == r[0].baseform for lu in lus for r in lu.readings]):
        if (aff == acc_value):
            return "tagsTrue"
        else:
            return "tagsFalse"
    elif any([''.join(tags) == ''.join(r[0].tags) or tags == r[0].tags for lu in lus for r in lu.readings]):
        if (aff == acc_value):
            return "lemmaTrue"
        else:
            return "lemmaFalse"
    else:
        return "both"

    
def test_is_analysed_correctly():
    assert is_analysed_correctly("кӗнекене",
                                 "кӗнеке",
                                 ["n", "dat"],
                                 APERTIUM_LANGS_SRC + "apertium-chv",
                                 "chv-morph") == True
    assert is_analysed_correctly("кӗнекене",
                                 "hargle",
                                 ["n", "dat"],                                 
                                 APERTIUM_LANGS_SRC + "apertium-chv",
                                 "chv-morph") == "lemma"
    assert is_analysed_correctly("кӗнекене",
                                 "кӗнеке",
                                 ["hargle"],                                 
                                 APERTIUM_LANGS_SRC + "apertium-chv",
                                 "chv-morph") == "tags"
    assert is_analysed_correctly("кӗнекене",
                                 "hargle",
                                 ["bargle"],                                 
                                 APERTIUM_LANGS_SRC + "apertium-chv",
                                 "chv-morph") == "both"


def lem_tags_2_lu(lem, tags):
    """(String (List of String)) -> String

    Given the lemma and the list of tags, return a lexical unit in Apertium
    stream format.
    """

    def format_tag(tag):
        return tag if tag.startswith("+") else "<" + tag + ">"
    return "^" + lem + "".join(format_tag(t) for t in tags) + "$"

def test_lem_tags_2_lu():
    assert lem_tags_2_lu("foo", ["n", "nom", "+i", "cop", "pres"]) ==\
        "^foo<n><nom>+i<cop><pres>$"


def apertium(inp, srcdir, mode):
    """ (String String String) -> String

    Run inp through 'apertium -d <srcdir> <mode>' command and return
    the result.
    """
    res = subprocess.run(['apertium', '-d', srcdir, mode],
                         stdout=subprocess.PIPE,
                         input=inp.encode('utf-8'))
    return res.stdout.decode('utf-8')

def test_apertium():
    assert apertium(".", APERTIUM_LANGS_SRC + "apertium-tat", "tat-morph") ==\
        "^../..<sent>$"


def chunks(l, n):
    """(List Int) -> (Generator List)
    Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]