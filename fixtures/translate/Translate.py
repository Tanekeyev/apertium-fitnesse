from waferslim.converters import convert_arg, convert_result, StrConverter

_STR_CONVERTER = StrConverter()

class Translate(object):
    def __init__(self):
        self._A = 0
        self._B = 0

    def setA(self, A):
        self._A = A

    def setB(self, B):
        self._B = B

    def concat(self):
        return self._A + self._B
