
import subprocess
from waferslim.converters import TableTableConstants as TTC

from streamparser.streamparser import parse

APERTIUM_LANGS_SRC = '../../apertium-languages/'
LANG1 = 'chv'
LANG2 = 'tat'
LANG3 = 'bak'
COLUMNS = 4

class Nonameyet(object):

    def do_table(self, table_rows):
        res = []
        for row in table_rows:
            res_row = []
            for lang, chunk in zip([LANG1, LANG2, LANG3], chunks(row, COLUMNS)):
                surf, lem, aff, tags = chunk
                srcdir = APERTIUM_LANGS_SRC + 'apertium-' + lang
                mode = lang + '-morph'
                iac = is_analysed_correctly(surf, lem, tags.split(), srcdir,
                                            mode)
                if iac == True:
                    res_row.extend([TTC.cell_no_change(),
                                    TTC.cell_correct(lem),
                                    TTC.cell_no_change(),
                                    TTC.cell_correct(tags)])
                elif iac == "lemma":
                    res_row.extend([TTC.cell_no_change(),
                                    TTC.cell_incorrect(lem),
                                    TTC.cell_no_change(),
                                    TTC.cell_correct(tags)])
                elif iac == "tags":
                    res_row.extend([TTC.cell_no_change(),
                                    TTC.cell_correct(lem),
                                    TTC.cell_no_change(),
                                    TTC.cell_incorrect(tags)])
                elif iac == "both":
                    res_row.extend([TTC.cell_no_change(),
                                    TTC.cell_incorrect(lem),
                                    TTC.cell_no_change(),
                                    TTC.cell_incorrect(tags)])
            res.append(tuple(res_row))
        return tuple(res)


def is_analysed_correctly(surf, lem, tags, srcdir, mode):
    """(String String (List of String) String String) -> True OR
                                                         OR "lemma"
                                                         OR "tags"
                                                         OR "both"

    Return True if the expected analysis (the lemma lem + tags) is among the
    analyses we get when we pass the surface form surf through the
    `apertium -d <srcdir> <mode>' command, "lemma" if there is one analysis with
    matching tags (i.e. the problem should be with the lemma) and "tags" if
    the lem is among the analyses, but with different tags and "both" if both
    don't match.
    """
    lus = list(parse(apertium(surf, srcdir, mode)))
    if any([lem == r[0].baseform and tags == r[0].tags for lu in lus
            for r in lu.readings]):
        return True
    elif any([lem == r[0].baseform for lu in lus for r in lu.readings]):
        return "tags"
    elif any([tags == r[0].tags for lu in lus for r in lu.readings]):
        return "lemma"
    else:
        return "both"
    
def test_is_analysed_correctly():
    assert is_analysed_correctly("кӗнекене",
                                 "кӗнеке",
                                 ["n", "dat"],
                                 APERTIUM_LANGS_SRC + "apertium-chv",
                                 "chv-morph") == True
    assert is_analysed_correctly("кӗнекене",
                                 "hargle",
                                 ["n", "dat"],                                 
                                 APERTIUM_LANGS_SRC + "apertium-chv",
                                 "chv-morph") == "lemma"
    assert is_analysed_correctly("кӗнекене",
                                 "кӗнеке",
                                 ["hargle"],                                 
                                 APERTIUM_LANGS_SRC + "apertium-chv",
                                 "chv-morph") == "tags"
    assert is_analysed_correctly("кӗнекене",
                                 "hargle",
                                 ["bargle"],                                 
                                 APERTIUM_LANGS_SRC + "apertium-chv",
                                 "chv-morph") == "both"


def lem_tags_2_lu(lem, tags):
    """(String (List of String)) -> String

    Given the lemma and the list of tags, return a lexical unit in Apertium
    stream format.
    """

    def format_tag(tag):
        return tag if tag.startswith("+") else "<" + tag + ">"
    return "^" + lem + "".join(format_tag(t) for t in tags) + "$"

def test_lem_tags_2_lu():
    assert lem_tags_2_lu("foo", ["n", "nom", "+i", "cop", "pres"]) ==\
        "^foo<n><nom>+i<cop><pres>$"


def apertium(inp, srcdir, mode):
    """ (String String String) -> String

    Run inp through 'apertium -d <srcdir> <mode>' command and return
    the result.
    """
    res = subprocess.run(['apertium', '-d', srcdir, mode],
                         stdout=subprocess.PIPE,
                         input=inp.encode('utf-8'))
    return res.stdout.decode('utf-8')

def test_apertium():
    assert apertium(".", APERTIUM_LANGS_SRC + "apertium-tat", "tat-morph") ==\
        "^../..<sent>$"


def chunks(l, n):
    """(List Int) -> (Generator List)
    Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

