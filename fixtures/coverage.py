
import subprocess
import re

class Coverage(object):

    def __init__(self):
        self.corpus = ''
        self.transducer = ''

    def set_corpus(self, corpus):
        self.corpus = corpus

    def set_transducer(self, transducer):
        self.transducer = transducer

    def coverage(self):
        testres = subprocess.run(['aq-covtest', self.corpus, self.transducer],
                                 stdout=subprocess.PIPE, encoding='utf8')
        cov = re.search(r"Coverage: *([0-9\.]*) *%", testres.stdout)
        return float(cov.group(1))
