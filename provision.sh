#!/usr/bin/env bash

## A script to set up apertium-fitnesse on a new Debian(-based) distro.
## Tested on Ubuntu 18.04.
##
## WARNING: clones apertium-all repository from Github
##          (few gigabytes of data)
##
## ASSUMPTIONS:
## - ssh access to Github from the machine:
##   (https://help.github.com/articles/connecting-to-github-with-ssh/)
## - Gitlab account
##   and the rights to clone gitlab.com/unhammer/turkiccorpora.git
## - sudo rights:
##   (sudo adduser <user> sudo)


#########################################
printf "Installing necessary packages\n\n"
#########################################

sudo apt-get --yes update && sudo apt-get --yes upgrade
sudo apt-get --yes install wget
wget https://apertium.projectjj.com/apt/install-nightly.sh -O - | sudo bash
sudo apt-get --yes install apertium-all-dev git subversion zip \
     python3-setuptools default-jdk webhook

############################################
printf "Cloning apertium-all repository\n\n"
############################################

git clone --recurse-submodules --shallow-submodules --depth 1 https://github.com/apertium/apertium-all.git

#######################################################################
printf "Compiling apertium-tat-eng for test purposes\n"
printf "This and other pairs will be set up to compile automatically\n"
printf "after each commit later on (via Travis or similar)\n\n"
#######################################################################

cd apertium-all/apertium-languages/apertium-tat
./autogen.sh && make
cd ../apertium-eng
./autogen.sh && make
cd ../../apertium-incubator/apertium-tat-eng
./autogen.sh --with-lang1=../../apertium-languages/apertium-tat --with-lang2=../../apertium-languages/apertium-eng && make
cd ../../../

######################################
printf "Cloning turkiccorpora repo\n\n"
######################################

git clone https://gitlab.com/unhammer/turkiccorpora.git
cd turkiccorpora
find . -name "*.bz2" -exec bzip2 -dk {} \;
find . -name "*.zip" -exec unzip {} \;
head -n 3083 tat.marriageRecord.csv > tat.marriageRecord.csv.firstHalf
tail -n 3083 tat.marriageRecord.csv > tat.marriageRecord.csv.secondHalf
cd ..

########################################
printf "Setting up apertium-quality\n\n"
########################################

cd apertium-all/apertium-tools/apertium-quality/mwtools/python3
sudo python3 ./setup.py install
cd ../../
./autogen.sh && make && sudo make install
cd ../../../

#################################
printf "Setting up waferslim\n\n"
#################################

wget https://files.pythonhosted.org/packages/65/60/d154ad7ebc4627238a24505871ccb6495e9dc1f71abe55e8516a65187203/waferslim-1.0.2-py3.1.zip
unzip waferslim-1.0.2-py3.1.zip
cd waferslim-1.0.2/
sudo python3 setup.py install
cd ../

################################################################################################
printf "Setting up Fitnesse and Webhooks (for triggering compile after each push to Github)\n\n"
################################################################################################

cd apertium-all/apertium-tools
git clone --recursive https://gitlab.com/selimcan/apertium-fitnesse.git
cp -r libs/streamparser/ streamparser
cd apertium-fitnesse
make getfitnesse
cd ../../../
cp apertium-all/apertium-tools/apertium-fitnesse/compile.sh ~
cp apertium-all/apertium-tools/apertium-fitnesse/hooks.json.in ~/hooks.json
printf "\nEdit hooks.json and fill in your Github secret code in there.\n\n"
nano hooks.json
webhook -hooks hooks.json -verbose -port 8080 &

cd apertium-all/apertium-tools/apertium-fitnesse
printf "\nEdit passwords.txt\n\n"
nano passwords.txt
sudo make launch
