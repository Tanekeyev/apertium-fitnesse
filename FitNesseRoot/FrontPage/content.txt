!1 Welcome to [[FitNesse][FitNesse.FitNesse]]!
!3 ''The fully integrated stand-alone acceptance testing framework and wiki.''

!c !3 This site is an unofficial acceptance testing framework/wiki for the [[Aperitum][http://www.apertium.org]] project. It runs [[Fitnesse][http://fitnesse.org]].
!c !3 It's main purpose is to give (non-tech-savvy) users an easy way of contributing their knowledge of natural languages, describe what functionality they want from a machine translator and its underlying components and thus help us build free/open-source machine translators!
!c !4 An example [[test page][.FitNesse.FullReferenceGuide.UserGuide.TwoMinuteExample]] you can run (just click on the 'Test' button). Except for that page, running tests on other pages is disabled/password-protected on this public server. If you're a developer, you can run an instance of Apertium-fitnesse on your machine as (will be) described at https://gitlab.com/selimcan/apertium-fitnesse and get automated tests for the translators you're working on for free.
!c !4 A [[test suite][.FrontPage.ApertiumTurkic]] for Apertium Turkic

|'''To Learn More...'''                                                                                    |
|[[User Guide][.FitNesse.UserGuide]]                           |''Answer the rest of your questions here.''|
|[[A Two-Minute Example][.FitNesse.UserGuide.TwoMinuteExample]]|''A brief example. Read this one next.''   |
|[[Acceptance Tests][.FitNesse.SuiteAcceptanceTests]]          |''FitNesse's suite of Acceptance Tests''   |
|[[Release Notes][.FitNesse.ReleaseNotes]]                     |''Find out about FitNesse's new features'' |

!define APERTIUM_SRC {../../../apertium-all/}
!define TURKICCORPORA_SRC {../../../turkiccorpora/}
!define CONFIGURATION_WAFERSLIM {
!define TEST_SYSTEM {slim}
!define SLIM_VERSION {0.1}
!define SLIM_PORT {8085}
!define COMMAND_PATTERN {/usr/bin/python3 -m waferslim.server}
}

#!define CONFIGURATION_JAVASLIM {
#!define TEST_SYSTEM {slim}
#!path javafixtures/
#|import  |
#|fixtures|
#}

!note Release ${FITNESSE_VERSION}
