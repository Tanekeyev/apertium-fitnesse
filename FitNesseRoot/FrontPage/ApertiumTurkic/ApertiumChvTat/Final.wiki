---
LastModifyingUser: selimcan
Test
---
${CONFIGURATION_WAFERSLIM}

!define CHV_TAT_SRC {${APERTIUM_SRC}apertium-incubator/apertium-chv-tat/}

| import |
| fixtures.coverage |

!1 ... with the end in mind

In the end, i.e. at the time of the final GSoC 2018 evaluation, which is on 6th-14th of August, we want three things from our Chuvash-Tatar translator:

 1 '''Performance on Wikipedia:''' It translates >95% of Chuvash and Tatar Wikipedias without errors ([[*/@/# symbols][http://wiki.apertium.org/wiki/Meaning_of_symbols_*_@_and_dieze_after_a_translation]]). We aim for getting it installed in Wikimedia's [[Content Translation][https://www.mediawiki.org/wiki/Content_translation]] tool by the time we're done. Wikipedia dumps are fetched from [[here][https://dumps.wikimedia.org/backup-index.html]] and plain text is extracted from them using the [[Wiki Extractor][http://wiki.apertium.org/wiki/WikiExtractor]] tool.
 1 '''Performance on a Tagged corpus:''' It analyses, disambiguates, translates and generates all sentences from the Chuvash/Tatar translation of Saint-Exupéry's "Little Prince" correctly (in both directions).
 1 '''Common Vocabulary: '''Dictionaries (apertium-chv.chv.lexc, apertium-tat.tat.lexc and apertium-chv-tat.chv-tat.dix) contain at least 12000 *noun/verb/adjective/adverb* stems. This number doesn't include closed-class words like numbers and it also does not include proper names. 

6th August might seem to be far away, but keep in mind that GSoC's duration is only three months, which is a mere 12 weeks or 60 working days.

Below are tests that soon will check all three things automatically (they already do, but only partially atm).

!1 Wikipedia
| Coverage |
| corpus | transducer | coverage? |
| ${TURKICCORPORA_SRC}cvwiki-latest-pages-articles.txt | ${CHV_TAT_SRC}chv-tat.automorf.bin | >=95 |
| ${TURKICCORPORA_SRC}ttwiki-latest-pages-articles.txt | ${CHV_TAT_SRC}tat-chv.automorf.bin | >=95 |

Under the hood, `Coverage' does the following: [[aq-covtest][http://wiki.apertium.org/wiki/Apertium-quality]] <corpus> <transducer> and returns the coverage number.
In addition to the above (i.e. 95% of words being analysed, read: without * symbols, no `@/#' errors should be left in the translation of Wikipedia in both directions. TODO automate this check too.

!1 Tagged corpus

!include .FrontPage.ApertiumTurkic.LePetitPrince.Chuvash

!1 Common vocabulary

!include .FrontPage.ApertiumTurkic.CommonVocabulary