---
LastModifyingUser: qavan
Test
secure-test
secure-write
---
${CONFIGURATION_WAFERSLIM}

TODO Clicking the 'Test' button ought to check whether the respective dictionary (a lexc file or bilingual dictionary) contains the stem and whether the stem is among translations when the ambiguity is exploded (see tat-eng-expolode-ambig mode for an example of that).

| Comment |
| # topic | Tatar | English gloss | Bashkir | Chuvash |
| numbers | бер | one | | |
| | ике | two | | |
| | өч | three | | |
| | дүрт | four | | |
| | биш | five | | |
| | алты | six | | |
| | җиде | seven | | |
| | сигез | eight | | |
| | тугыз | nine | | |
| | ун | ten | | |
| | унбер | eleven | | |
| | унике | twelve | | |
| | унөч | thirteen | | |
| | ундүрт | fourteen | | |
| | унбиш | fifteen | | |
| | уналты | sixteen | | |
| | унҗиде | seventeen | | |
| | унсигез | eighteen | | |
| | унтугыз | nineteen | | |
| | егерме | twenty | | |
| | утыз | thirty | | |
| | кырык | forty | | |
| | илле | fifty | | |
| | алтмыш | sixty | | |
| | җитмеш | seventy | | |
| | сиксән | eighty | | |
| | туксан | ninety | | |
| | йөз | hundred | | |
| | мең | thousand | | |
| | миллион | million | | |
| | миллиард | billion | | |
| | триллион | trillion | | |
| | триллиард | trilliard | | |
| postpositions | буларак | as | | |
| | аркылы | through | | |
| | аша | through | | |
| | бәрабәренә | at the cost of | | |
| | белән               (alt берлән) | with | | |
| | буе | all | | |
| | буена | at | | |
| | буенда | at | | |
| | буенча | along | | |
| | буйлап | along | | |
| | илә                 (alt илән) | with | | |
| | кебек | like | | |
| | күк | like | | |
| | өчен | for | | |
| | саен | every | | |
| | сыман | like | | |
| | турында | about | | |
| | хакында | about | | |
| | шикелле | about | | |
| | кадәр | till | | |
| | тикле | till | | |
| | хәтле | till | | |
| | чаклы | till | | |
| | караганда | in comparison to | | |
| | карамастан | despite | | |
| | карамый | despite | | |
| | карата | with regard to | | |
| | каршы | against;to | | |
| | күрә | because | | |
| | күрә | in comformity with | | |
| | таба | to;towards | | |
| | якын | about | | |
| | ары | after | | |
| | башка | without | | |
| | бирле | since | | |
| | бүтән | except;besides | | |
| | соң | after | | |
| | тыш | besides | | |
| | элек | before | | |
| | бергә | with | | |
| | беррәттән | along with | | |
| conjunctions | белән (alt берлән) (alt берлә) | and | | |
| | вә | and {archaic}) | | |
| | да:~да (alt дә) (alt та) (alt тә) | and | | |
| | ни | neither... , nor ... (x2) | | |
| | плүс (alt плюс) | plus | | |
| | шулай ук | likewise | | |
| | һәм | and | | |
| | бары тик | | | |
| | бәлки | | | |
| | вәләкин | | | |
| | ләкин | | | |
| | мәгәр | | | |
| | тик | | | |
| | фәкать | | | |
| | хәлбуки | | | |
| | ә | | | |
| | әмма | | | |
| | я | | | |
| | яисә | | | |
| | яки | | | |
| | яхут | | | |
| | әле | | | |
| | гүя | | | |
| | гүяки | | | |
| | гәрчә | | | |
| | ки | | | |
| | югыйсә | | | |
| | юкса | | | |
| | ягъни | | | |
| | әгәр дә | | | |
| | әгәр | | | |
| | алай да | | | |
| | алайса | | | |
| | аның каравы | | | |
| | әйтүләренчә | | | |
| | башкача әйткәндә | | | |
| | белдерүенчә | | | |
| | әйтүенчә | | | |
| | мәгълүматынча | | | |
| | сүзенчә | | | |
| | сүзләренчә | | | |
| | уенча | | | |
| | уйлавынча | | | |
| | фикеренчә | | | |
| | хәбәр итүенчә | | | |
| | шулай да | | | |
| | шул сәбәпле | | | |
| | шунлыктан | | | |
| | шуңа күрә | | | |
| | язуынча | | | |

+ 1000 nouns/verbs/adjectives/adverbs (of your choice, but keeping an eye on the weekly Wikipedia coverage goal).
