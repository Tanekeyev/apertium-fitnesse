getfitnesse:
	wget https://search.maven.org/remotecontent?filepath=org/fitnesse/fitnesse/20180127/fitnesse-20180127-standalone.jar -O fitnesse-20180127-standalone.jar
clean:
	find FitNesseRoot -name "*.zip" -delete
launch:
	java -jar fitnesse-20180127-standalone.jar -p 8090
