#!/usr/bin/env bash

## A script to set up apertium-fitnesse  on Ubuntu 16.04



#########################################
printf "Installing necessary packages\n\n"
#########################################

sudo apt-get --yes update && sudo apt-get --yes upgrade
sudo apt-get --yes install wget
wget https://apertium.projectjj.com/apt/install-nightly.sh -O - | sudo bash
sudo apt-get --yes install apertium-all-dev git subversion zip \
     python3-setuptools default-jdk 

git clone https://github.com/GabidenTanekeev/lexc2dix.git
cd lexc2dix
sudo python3 ./setup.py install
cd ../

sudo pip3 install bs4
#########################################
printf "Installing webhook\n\n"
#########################################

wget https://github.com/adnanh/webhook/releases/download/2.6.6/webhook-linux-amd64.tar.gz
tar -xvf webhook-linux-amd64.tar.gz
sudo mv webhook-linux-amd64/webhook /usr/local/bin
rm -rf webhook-linux-amd64*

############################################
printf "Cloning apertium-kaz , apertium-tools\n\n"
############################################

mkdir apertium-all
sudo chmod -R 777 apertium-all
cd apertium-all
git clone --recurse-submodules --depth 1 git@github.com:apertium/apertium-tools.git
mkdir apertium-languages
cd apertium-languages
sudo chmod -R 777 apertium-languages
git clone --recurse-submodules --depth 1 git@github.com:apertium/apertium-kaz.git
cd ../../

#######################################################################
printf "Compiling apertium-tat-eng for test purposes\n"
printf "This and other pairs will be set up to compile automatically\n"
printf "after each commit later on (via Travis or similar)\n\n"
#######################################################################
cd apertium-all/apertium-languages/apertium-kaz
./autogen.sh && make

########################################
printf "Setting up apertium-quality\n\n"
########################################
cd ../../../
cd apertium-all/apertium-tools/apertium-quality/mwtools/python3
sudo python3 ./setup.py install
cd ../../
./autogen.sh && make && sudo make install
cd ../../../

#################################
printf "Setting up waferslim\n\n"
#################################

wget https://files.pythonhosted.org/packages/65/60/d154ad7ebc4627238a24505871ccb6495e9dc1f71abe55e8516a65187203/waferslim-1.0.2-py3.1.zip
unzip waferslim-1.0.2-py3.1.zip
cd waferslim-1.0.2/
sudo python3 setup.py install
cd ../

################################################################################################
printf "Setting up Fitnesse and Webhooks (for triggering compile after each push to Github)\n\n"
################################################################################################

cd apertium-all/apertium-tools
git clone --recursive https://gitlab.com/Tanekeyev/apertium-fitnesse.git
#sudo cp -r streamparser/ apertium-fitnesse/
#cd apertium-fitnesse/streamparser
#sudo python3 ./setup.py install
#cd ../../
sudo chmod -R 777 apertium-fitnesse
cd apertium-fitnesse
make getfitnesse
git clone https://github.com/GabidenTanekeev/streamparser.git
cd streamparser/
sudo python3 ./setup.py install
cd ../../../../
cp apertium-all/apertium-tools/apertium-fitnesse/compile.sh ~
cp apertium-all/apertium-tools/apertium-fitnesse/hooks.json.in ~/hooks.json


################################################################################################
printf "lexc2dix\n\n"
################################################################################################

lexc2dix apertium-all/apertium-languages/apertium-kaz/apertium-kaz.kaz.lexc

webhook -hooks hooks.json -verbose -port 8080 &
cd apertium-all/apertium-tools/apertium-fitnesse
sudo make launch



