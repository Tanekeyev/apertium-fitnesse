This site is an unofficial acceptance testing framework/wiki for the Aperitum
project. It is based on Fitnesse_.

It's main purpose is to give (non-tech-savvy) users an easy way of contributing
their knowledge of natural languages, describe what functionality they want from
a machine translator and its underlying components and thus help Apertiumers
build free/open-source machine translators!

If you're an Apertium developer, you can run an instance of Apertium-fitnesse on
your machine by running provision.sh and get automated tests for the translators
you're working on for free.

Note that provision.sh downloads all of the apertium-* repos, which will take a
lot of bandwith.

If you're interested only in some of the Apertium language modules or
translators, you just might clone the module you're interested in into
either apertium-all/apertium-languages or apertium-all/apertium-incubator/nursery/...
and clone this repo into apertium-all/apertium-tools/, install the dependencies
as described in provision.sh and then launch apertium-fitnesse with
`make launch'.

SampleOutput.png is the output you should expect as of 07.06.2017.
SampleOutput2.png and SampleOutput3.png are more recent examples from 28.02.2018.

An instance of apertium-fitnesse runs on fitnesse.selimcan.org
(running tests is protected by a password).

.. _Fitnesse: http://fitnesse.org
